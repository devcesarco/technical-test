<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\RegisterController;
use App\Http\Controllers\SesionsController;
use App\Http\Controllers\ProductsController;


Route::get('/', function () {
    return view('home');
})->middleware('auth');

Route::get('/register', [RegisterController::class, 'create'])
    ->middleware('guest')
    ->name('register.index');

Route::post('/register', [RegisterController::class, 'store'])
    ->name('register.store');



Route::get('/login', [SesionsController::class, 'create'])
    ->middleware('guest')
    ->name('login.index');

Route::post('/login', [SesionsController::class, 'store'])

    ->name('login.store');

Route::get('/logout', [SesionsController::class, 'destroy'])
    ->middleware('auth')
    ->name('login.destroy');

Route::get('/products', [ProductsController::class, 'home'])
    ->name('products.home');
;