@extends('layouts.app')
@section('title','Home')
@section('content')

    <h1 class="text-5xl text-center">Welcome to ShopOnline</h1>

    <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">

        <table class="table-fixed w-full">
          <thead>
            <tr class="bg-indigo-500 text-white">
              <th class="w-20 py-4 ...">ID</th>
              <th class="w-1/8 py-4 ...">Title</th>
              <th class="w-1/16 py-4 ...">Description</th>
              <th class="w-1/16 py-4 ...">Price</th>
              <th class="w-1/16 py-4 ...">Update</th>
              <th class="w-28 py-4 ...">Actions</th>
            </tr>
          </thead>
          <tbody>
              
            <tr>
              <td class="py-3 px-6">858</td>
              <td class="p-3">Portatil Asus</td>
              <td class="p-3 text-center">CPU AMD 12 - RAM 12 - SSD 500 GB</td>
              <td class="p-3 text-center">$ 1350000 COP</td>
              <td class="p-3">
                <button class="bg-red-500 text-white px-3 py-1 rounded-sm">
                <i class="fas fa-trash"></i></button>
                <button class="bg-green-500 text-white px-3 py-1 rounded-sm">
                <i class="fas fa-pen"></i></button>
              </td>
            </tr>
            
            
          </tbody>
        </table>
    </div>

@endsection 