@extends('layouts.app')
@section('title','Register')
@section('content')


<div class="block mx-auto my-12 p-8 bg-white w-1/4 border border-gray-200 rounded-lg shadow-lg">
    <h1 class="text-5xl text-center font-bold">Register</h1>

    <form class="mt-4" method="POST" action="">

        @csrf

        <input type="text" name="name" id="name" placeholder="Name" class="border border-gray-200 rounded-md bg-gray-200 w-full
        text-lg placeholder-gray-382 p-2 my-2 focus:bg-white ">
        @error('name')
        <p class="border border-red-500 rounded-md bg-red-100 w-full text-orange-600 p-2 my-2">
            *{{ $message}}
        </p>
    @enderror

        <input type="email" name="email" id="email" placeholder="Email" class="border border-gray-200 rounded-md bg-gray-200 w-full
        text-lg placeholder-gray-382 p-2 my-2 focus:bg-white ">

        @error('email')
        <p class="border border-red-500 rounded-md bg-red-100 w-full text-orange-600 p-2 my-2">
            *{{ $message}}
        </p>
        @enderror

        <input type="password" name="password" id="password" placeholder="Password" class="border border-gray-200 rounded-md 
        bg-gray-200 w-full text-lg placeholder-gray-382 p-2 my-2 focus:bg-white ">
        @error('password')
        <p class="border border-red-500 rounded-md bg-red-100 w-full text-orange-600 p-2 my-2">
            *{{ $message}}
        </p>
    @enderror
        <input type="password" name="Re-password" id="Re-password" placeholder="Re-Password" class="border border-gray-200 
        rounded-md bg-gray-200 w-full text-lg placeholder-gray-382 p-2 my-2 focus:bg-white ">

        <button type="submit" class="rounded-md bg-green-500 w-full text-lg 
        text-white font.semibold p-2 my-3 hover:bg-green-600 focus:">Send</button>

    </form>
</div>
@endsection