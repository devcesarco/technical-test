<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>@yield('title') - ShopOnline</title>

    <!-- Tailwind CSS Link -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tailwindcss/2.0.1/tailwind.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://kit.fontawesome.com/a23e6feb03.js"></script>
</head>

<body class="bg-gray-100 text-gray-800">

    <nav class="flex py-5 bg-green-700 text-white">
        <div class="w-1/2 px-12 mr-auto">
            <p class="text-2xl font-bold">ShopOnline</p>
        </div>

        @if(auth()->check())

        <ul class="w-1/2 px-16 ml-auto flex justify-end pt-1">
            <li class="mx-8">
                <p class="text.xl">Welcome <b>{{ auth()->user()->name}}</b></p>
            </li>
            <li>
                <a href="{{ route('login.destroy') }}" class="font-bold 
                 py-3 px-4 rounded-md hover:bg-green-400 ">Log out</a>

            </li>
            @else
            <li>
                <a href="{{ route('login.index') }}" class="font-semibold hover:bg-green-400
                 py-3 px-4 rounded-md">Log in</a>

                <a href="{{ route('register.index') }}" class="font-semibold 
                 py-3 px-4 rounded-md hover:bg-green-400 ">Log up</a>

            </li>
            @endif
        </ul>

    </nav>
    <main class="p-16 flex justify-center">
        @yield('content')

    </main>



</body>

</html>